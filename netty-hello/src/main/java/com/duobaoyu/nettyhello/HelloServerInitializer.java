package com.duobaoyu.nettyhello;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpServerCodec;

/**
 * @author guiguan
 * @date 2019/9/26 16:54
 */
public class HelloServerInitializer extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel channel) throws Exception {
        //获取channel下的所有pipeline
        ChannelPipeline pipeline = channel.pipeline();
        //添加一个handler命名为 HttpServerCodec
        //HttpServerCodec 是由netty提供的助手类，可以理解为拦截器，服务端做解码，客户端做编码
        pipeline.addLast("HttpServerCodec",new HttpServerCodec());
        //添加自定义的注解类
        pipeline.addLast("CustomHandler",new CustomHandler());
    }
}
