package com.duobaoyu.nettyhello;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * 实现客户端发送一个请求，服务器会返回 hello netty
 *
 * @author guiguan
 * @date 2019/9/26 15:09
 */
public class HelloServer {

    public static void main(String[] args) {
        //定义一对线程组
        //主线程组，用于接受客户端的连接，但是不做任何处理，跟老板一样，不做事
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        //从线程组，老板线程组会把任务丢给他，让手下线程组去做任务
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            //netty服务器的创建
            serverBootstrap
                    //设置主从线程组 在netty内部是父子线程组
                    .group(bossGroup, workerGroup)
                    //设置nio的双向通道
                    .channel(NioServerSocketChannel.class)
                    //和childOption 逻辑相同，在channel建立时做特殊处理
                    .option(ChannelOption.SO_BACKLOG, 1024)
                    //子处理器，用于处理workGroup
                    .childHandler(new HelloServerInitializer());

            //启动server，并且设置8088为启动的端口号，同时启动方式为同步 返回异步future
            ChannelFuture channelFuture = serverBootstrap.bind(8088).sync();
            //监听关闭的channel，设置为同步方式
            channelFuture.channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //优雅退出
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }


    }
}
