package com.duobaoyu.websocket;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.time.LocalDateTime;

/**
 * 处理消息的handler
 *
 * @author guiguan
 * @date 2019/9/29 9:58
 * TextWebSocketFrame：在netty中，是用于为websocket专门处理文本的对象，frame是消息的载体
 */
public class ChatHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {


    /**
     * 用于记录和管理所有客户端的channel
     */
    private static ChannelGroup clients = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, TextWebSocketFrame msg) throws Exception {
        //获取客户端传输过来的消息
        String content = msg.text();
        System.out.println("接收到的数据：" + content);

        for (Channel client : clients) {
            client.writeAndFlush(new TextWebSocketFrame("服务器在" + LocalDateTime.now() + "接收到消息，消息为：" + content));
        }


        //下面这个方法和上面的for循环一直
//        clients.writeAndFlush("服务器在" + LocalDateTime.now() + "接收到消息，消息为：" + content);
    }


    /**
     * 当呵护短连接服务端后（打开链接）
     * 获取客户端的channel，并且放到ChannelGroup中去进行管理
     *
     * @param ctx ChannelHandlerContext
     * @throws Exception Exception
     */
    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        clients.add(ctx.channel());
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        //当触发handlerRemoved，ChannelGroup 会自动移除对应客户端的channel
        System.out.println("channel对应的长id为：" + ctx.channel().id().asLongText());
        //短id可能重复
        System.out.println("channel对应的短id为：" + ctx.channel().id().asShortText());
    }
}
