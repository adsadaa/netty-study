package com.duobaoyu.niodemo;

/**
 * @author guiguan
 * @date 2019/9/25 14:41
 */
public class TimeClient {


    public static void main(String[] args) {
        int port = 8080;
        try {
            port = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            //采用默认值
        }
        //此处异步处理
        new Thread(new TimeClientHandler("127.0.0.1", port), "TimeClient-001").start();
    }
}
