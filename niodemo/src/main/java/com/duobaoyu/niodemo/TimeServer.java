package com.duobaoyu.niodemo;

/**
 * @author guiguan
 * @date 2019/9/20 14:37
 */
public class TimeServer {

    public static void main(String[] args) {
        int port = 8080;
        if (args != null && args.length > 0) {
            try {
                port = Integer.valueOf(args[0]);
            }catch (NumberFormatException e){
                //采用默认值
            }
        }
        MultiplexerTimeServer timeServer = new MultiplexerTimeServer(port);
        //异步启动
        new Thread(timeServer,"NIO-MultiplexerTimeServer-001").start();
    }
}
