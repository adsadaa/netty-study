package com.duobaoyu.socketdemo;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 1.阻塞io，如果客户端传递消息传递了60s，则服务端需要等待60s
 * 2.每个客户端链接都是一个线程，高并发下会宕机
 * 3.如果使用线程池，则后续线程都会阻塞
 * 4.由于所有链接都超时，客户端会不停重试，服务端响应会越来越慢，进入死循环
 *
 *
 * @author guiguan
 * @date 2019/9/18 10:22
 */
public class TimeServer {

    public static void main(String[] args) throws IOException {
        int port = 8080;
        if (args != null && args.length > 0) {
            try {
                port = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                //采用默认值
            }
        }
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("The time server is start in port:" + port);
            Socket socket = null;
            while (true) {
                //bio 的阻塞就体现在该accept方法上
                socket = serverSocket.accept();
                new Thread(new TimeServerHandler(socket)).start();
            }
        } finally {
            if (serverSocket != null) {
                System.out.println("The time server close");
                serverSocket.close();
                serverSocket = null;
            }
        }
    }
}
