package com.duobaoyu.chatnetty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChatNettyApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChatNettyApplication.class, args);
    }

}
