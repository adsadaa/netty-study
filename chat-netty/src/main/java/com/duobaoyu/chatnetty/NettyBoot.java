package com.duobaoyu.chatnetty;

import com.duobaoyu.chatnetty.netty.WsServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * netty启动类
 * Spring首次创建上下文时启动
 *
 * @author guigu
 */
@Slf4j
@Component
public class NettyBoot implements ApplicationListener<ContextRefreshedEvent> {

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        //当root application context启动时就启动netty，root applicationContext没有父上下文
        //springMvc的项目会有两个上下文，一个是 root applicationContext,一个是projectServlet Context
        if (event.getApplicationContext().getParent() == null) {
            try {
                WsServer.getInstance().start();
            } catch (Exception e) {
                log.error("netty启动失败", e);
            }
        }
    }

}
