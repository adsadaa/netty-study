package com.duobaoyu.chatnetty.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.springframework.stereotype.Component;

/**
 * @author guigu
 */
@Component
public class WsServer {

    private ServerBootstrap server;

    private static class SingletonWsServer {
        static final WsServer INSTANCE = new WsServer();
    }

    /**
     * 将server做成单例
     *
     * @return WsServer
     */
    public static WsServer getInstance() {
        return SingletonWsServer.INSTANCE;
    }


    public WsServer() {
        EventLoopGroup mainGroup = new NioEventLoopGroup();
        EventLoopGroup subGroup = new NioEventLoopGroup();
        server = new ServerBootstrap();
        server.group(mainGroup, subGroup).channel(NioServerSocketChannel.class).childHandler(new WsServerInitializer());
    }

    public void start() {
        ChannelFuture future = server.bind(8088);
        System.err.println("netty websocket server 启动完毕...");
    }
}
