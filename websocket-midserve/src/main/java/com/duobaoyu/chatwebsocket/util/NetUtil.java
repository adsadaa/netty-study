package com.duobaoyu.chatwebsocket.util;

import lombok.extern.slf4j.Slf4j;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 网络 utils
 *
 * @author ningchang
 */
@Slf4j
public class NetUtil {

    /**
     * 获取内网ip
     *
     * @author ningchang
     */
    public static String getHostIp() {
        String netAddress = null;
        try {
            netAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            log.error(" ----------------------- get host ip address error ------------------------- ");
            System.exit(0);
        }
        return netAddress;
    }

}
