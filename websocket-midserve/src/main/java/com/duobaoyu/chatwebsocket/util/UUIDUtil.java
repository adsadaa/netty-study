package com.duobaoyu.chatwebsocket.util;

import jodd.util.StringPool;

import java.util.UUID;

/**
 * @author guiguan
 */
public class UUIDUtil {

    public static String getUUID() {
        return UUID.randomUUID().toString().replace(StringPool.DASH, StringPool.EMPTY);
    }
}


