package com.duobaoyu.chatwebsocket.util;

import com.alibaba.fastjson.JSON;

/**
 * @author guiguan
 */
public class JsonObjectUtil {

    public static <T> T parseObject(Object obj, Class<T> clazz) {
        String json = JSON.toJSONString(obj);
        return JSON.parseObject(json, clazz);
    }
}
