package com.duobaoyu.chatwebsocket.constant;

/**
 * @author mingfang
 */
public interface RedisConstant {

    /**
     * 员工channelId关系
     */
    String EMP_CHANNEL_RELATION = "EMP_CHANNEL_RELATION:";
}
