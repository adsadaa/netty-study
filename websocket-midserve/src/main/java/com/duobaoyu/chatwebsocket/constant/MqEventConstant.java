package com.duobaoyu.chatwebsocket.constant;

/**
 * @author guiguan
 */
public interface MqEventConstant {

    /**
     * 来自客户的消息
     */
    String USER_TO_EMP = "user_msg_chatsystem";

    /**
     * 顾问消息处理结果
     */
    String MESSAGE_RESULT = "msg_result_chatsystem";

    /**
     * 需要登录
     */
    String NEED_LOGIN = "need_login_chatsystem";

    /**
     * 多端消息同步
     */
    String SYNC_MSG = "msg_sync_chatsystem";

    /**
     * 游客登录结果
     */
    String USER_CONNECT_RESULT="user_connect_result";

    /**
     * 顾问消息 ---> 头条客户
     */
    String MESSAGE_TO_CLIENT_WEBSOCKET= "message_to_client_websocket";

    /**
     * 用户消息处理结果
     */
    String CLIENT_MESSAGE_RESULT="client_message_result";

    /**
     * 标签事件
     */
    String LABEL_MSG="label_msg";

    /**
     * 顾问关闭接客开关事件
     */
    String SALE_CLOSE_RECEPTION_SWITCH = "sale_close_reception_switch";

    /**
     * 顾问打开接客开关事件
     */
    String SALE_OPEN_RECEPTION_SWITCH = "sale_open_reception_switch";
}
