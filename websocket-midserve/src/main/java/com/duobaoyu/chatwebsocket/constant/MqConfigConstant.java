package com.duobaoyu.chatwebsocket.constant;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


/**
 * 因为spring的value注解在构造函数加载之后，所以必须要使用配置常量类
 *
 * @author gg
 * @version MqConfigConstant.java, v 0.1 2019-08-09 11:48 gg
 */
@Getter
@Component
public class MqConfigConstant {

    @Value(value = "${rocket.mq.accessKey}")
    private String accessKey;

    @Value(value = "${rocket.mq.secretKey}")
    private String secretKey;

    @Value(value = "${rocket.mq.nameSrvAddr}")
    private String nameSrvAddr;

    @Value(value = "${rocket.mq.topic}")
    private String topic;

    @Value(value = "${rocket.mq.groupId}")
    private String groupId;

    @Value(value = "${rocket.mq.consumeTopic}")
    private String consumeTopic;

}