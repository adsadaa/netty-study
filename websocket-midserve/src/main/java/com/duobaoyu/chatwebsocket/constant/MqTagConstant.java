package com.duobaoyu.chatwebsocket.constant;

/**
 * @author guiguan
 */
public interface MqTagConstant {

    /**
     * 来自顾问的消息
     */
    String EMP_MSG = "sale_msg_websocket";

    /**
     * 移除长连接信息
     */
    String REMOVE_CHANNEL = "disconnect_websocket";

    /**
     * 添加长连接信息
     */
    String ADD_CHANNEL = "connect_websocket";

    /**
     * 签收语音
     */
    String SIGN_VOICE = "sign_voice";

    /**
     * 签收所有
     */
    String SIGN_ALL = "sign_all";

    /**
     * socket接收tag
     */
    String TO_SOCKET_BROADCASTING = "to_socket_broadcasting";

    /**
     * 游客建立连接
     */
    String CLIENT_CONNECT_WEBSOCKET="client_connect_websocket";

    /**
     * 来自头条游客的信息
     */
    String CLIENT_MSG="client_msg";
}
