package com.duobaoyu.chatwebsocket;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author guiguan
 * @date 2019/10/12
 */
@Slf4j
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
public class WebSocketMidServeApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebSocketMidServeApplication.class, args);
        log.info("webSocketMidServeApplication working");
    }

}
