package com.duobaoyu.chatwebsocket.producer;

import com.alibaba.fastjson.JSON;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.SendResult;
import com.aliyun.openservices.ons.api.bean.ProducerBean;
import com.duobaoyu.chatwebsocket.constant.MqConfigConstant;
import com.duobaoyu.chatwebsocket.constant.RedisConstant;
import com.duobaoyu.chatwebsocket.dto.EmpChannelMsgDto;
import com.duobaoyu.chatwebsocket.dto.WebSocketMessageDto;
import com.duobaoyu.chatwebsocket.dto.WebSocketMqDto;
import com.duobaoyu.chatwebsocket.netty.UserChannelRel;
import com.duobaoyu.chatwebsocket.util.SpringUtil;
import com.duobaoyu.chatwebsocket.util.UUIDUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author guiguan
 * @date 2019/10/9 15:18
 */
@Slf4j
@Component
public class WebSocketProducer {

    /**
     * 顾问消息
     *
     * @param webSocketMqDto WebSocketMqDto
     * @param tag            tag
     */
    public void sendMsgToChatSystem(WebSocketMqDto webSocketMqDto, String tag) {
        Message message = new Message();
        MqConfigConstant mqConfigConstant = SpringUtil.getBean(MqConfigConstant.class);
        message.setTopic(mqConfigConstant.getTopic());
        message.setTag(tag);
        String uuid = UUIDUtil.getUUID();
        message.setMsgID(uuid);
        message.setKey(uuid);
        message.setBody(JSON.toJSONString(webSocketMqDto).getBytes());
        log.info("sendMsgToChatSystem,message:{} \n webSocketMqDto:{}", message, webSocketMqDto);
        //mq 异步处理消息
        ProducerBean producerBean = SpringUtil.getBean(ProducerBean.class);
        SendResult send = producerBean.send(message);
        log.info("sendMsgToChatSystem SendResult:{}", send);
    }


    /**
     * 创建连接
     *
     * @param empChannelMsgDto EmpChannelMsgDto
     */
    public void connect(EmpChannelMsgDto empChannelMsgDto) {
        List<String> channelIdList = new ArrayList<>();
        channelIdList.add(empChannelMsgDto.getChannelId());
        log.info("connect_empChannelMsgDto : {}",JSON.toJSON(empChannelMsgDto));
        //获取缓存的channelId
        ConcurrentHashMap<String, List<String>> empIdWithChannelList = UserChannelRel.empIdWithChannelList;
        List<String> channelIds = empIdWithChannelList.get(RedisConstant.EMP_CHANNEL_RELATION.concat(empChannelMsgDto.getEmpId()));
        if(CollectionUtils.isNotEmpty(channelIds)){
            log.info("channelIds : {}",JSON.toJSON(channelIds));
            channelIdList.addAll(channelIds);
        }
        empIdWithChannelList.put(RedisConstant.EMP_CHANNEL_RELATION.concat(empChannelMsgDto.getEmpId()),channelIdList);
    }

    /**
     * 断开连接
     *
     * @param empChannelMsgDto EmpChannelMsgDto
     */
    public void disconnect(EmpChannelMsgDto empChannelMsgDto) {
        log.info("disconnect_empChannelMsgDto : {}",JSON.toJSON(empChannelMsgDto));
        ConcurrentHashMap<String, List<String>> empIdWithChannelList = UserChannelRel.empIdWithChannelList;
        log.info("empIdWithChannelList : {}",JSON.toJSON(empIdWithChannelList.get(RedisConstant.EMP_CHANNEL_RELATION.concat(empChannelMsgDto.getEmpId()))));
        String key = RedisConstant.EMP_CHANNEL_RELATION.concat(empChannelMsgDto.getEmpId());
        List<String> channelIdList = empIdWithChannelList.get(key);
        if(CollectionUtils.isNotEmpty(channelIdList)){
            channelIdList.remove(empChannelMsgDto.getChannelId());
            log.info("empIdWithChannelList_remove : {}",JSON.toJSON(empIdWithChannelList.get(key)));
        }
        empIdWithChannelList.put(key,channelIdList);
    }

    public void sign(WebSocketMessageDto webSocketMessageDto,String tag) {
        Message message = new Message();
        MqConfigConstant mqConfigConstant = SpringUtil.getBean(MqConfigConstant.class);
        message.setTopic(mqConfigConstant.getTopic());
        message.setTag(tag);
        String uuid = UUIDUtil.getUUID();
        message.setMsgID(uuid);
        message.setBody(JSON.toJSONString(webSocketMessageDto).getBytes());
        log.info("sign,message:{}", message);
        //mq 异步处理消息
        ProducerBean producerBean = SpringUtil.getBean(ProducerBean.class);
        SendResult send = producerBean.send(message);
        log.info("sign SendResult:{}", send);
    }
    public void sendUserToChatSystem(WebSocketMqDto webSocketMqDto, String tag) {
        Message message = new Message();
        MqConfigConstant mqConfigConstant = SpringUtil.getBean(MqConfigConstant.class);
        message.setTopic(mqConfigConstant.getTopic());
        message.setTag(tag);
        String uuid = UUIDUtil.getUUID();
        message.setMsgID(uuid);
        message.setKey(uuid);
        message.setBody(JSON.toJSONString(webSocketMqDto).getBytes());
        log.info("sendMsgToChatSystem,message:{} \n webSocketMqDto:{}", message, webSocketMqDto);
        //mq 异步处理消息
        ProducerBean producerBean = SpringUtil.getBean(ProducerBean.class);
        SendResult send = producerBean.send(message);
        log.info("sendMsgToChatSystem SendResult:{}", send);
    }



}
