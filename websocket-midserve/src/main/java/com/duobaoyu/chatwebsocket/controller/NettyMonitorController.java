package com.duobaoyu.chatwebsocket.controller;

import com.duobaoyu.chatwebsocket.dto.EmpChannelMsgDto;
import com.duobaoyu.chatwebsocket.netty.UserChannelRel;
import com.duobaoyu.chatwebsocket.util.NetUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import io.netty.channel.Channel;
import net.sf.json.JSONObject;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.SocketAddress;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * netty监控类
 *
 * @author guiguan
 * @date 2019/10/14 15:14
 */
@RestController
public class NettyMonitorController {

    @GetMapping("monitor")
    public Object monitorBase() {
        JSONObject monitorBase = new JSONObject();

        //服务器连接数
        int size = UserChannelRel.users.size();
        monitorBase.put("长连接数", size);
        //内网ip
        String ip = NetUtil.getHostIp();
        monitorBase.put("服务器ip", ip);
        ConcurrentHashMap<Channel, EmpChannelMsgDto> channelEmpManager = UserChannelRel.channelEmpManager;

        //长连接信息集合
        List<EmpChannelMsgDto> empChannelMsgDtoList = Lists.newArrayList();
        channelEmpManager.forEach((key, value) -> {
            empChannelMsgDtoList.add(value);
        });

        Map<String, List<EmpChannelMsgDto>> empMap = Maps.newHashMap();
        empChannelMsgDtoList.forEach(empChannelMsgDto -> {
            List<EmpChannelMsgDto> empChannelMsgDtos = empMap.get(empChannelMsgDto.getEmpId());
            if (CollectionUtils.isEmpty(empChannelMsgDtos)) {
                empChannelMsgDtos = Lists.newArrayList(empChannelMsgDto);
            } else {
                empChannelMsgDtos.add(empChannelMsgDto);
            }
            empMap.put(empChannelMsgDto.getEmpId(), empChannelMsgDtos);
        });
        monitorBase.put("顾问连接信息", empMap);

        Map<String, JSONObject> channelMap = Maps.newHashMap();
        Set<String> remoteIpSet = Sets.newHashSet();

        for (Channel channel : UserChannelRel.users) {
            String remoteIp = channel.remoteAddress().toString();
            String localIp = channel.localAddress().toString();
            EmpChannelMsgDto empChannelMsgDto = channelEmpManager.get(channel);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("remoteIp", remoteIp);
            jsonObject.put("localIp", localIp);
            jsonObject.put("connect", empChannelMsgDto != null);
            if (empChannelMsgDto != null) {
                String empId = empChannelMsgDto.getEmpId();
                jsonObject.put("empId", empId);
            }
            jsonObject.put("channelId", channel.id().asLongText());
            channelMap.put(channel.id().asLongText(), jsonObject);
            remoteIpSet.add(remoteIp);
        }

        monitorBase.put("所有长连接信息", channelMap);
        monitorBase.put("remoteIpSet", remoteIpSet);
        return monitorBase;
    }
}
