package com.duobaoyu.chatwebsocket.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 长连接信息dto
 *
 * @author guiguan
 * @date 2019/10/14 11:16
 */
@Data
public class EmpChannelMsgDto implements Serializable {

    private String empId;

    /**
     * 长id
     */
    private String channelId;

    /**
     * 短id
     */
    private String shortId;

    /**
     * 长连接服务器ip
     */
    private String ip;

    /**
     * 登录用户的公网ip
     */
    private String remoteIp;
}
