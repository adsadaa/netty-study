package com.duobaoyu.chatwebsocket.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 该对象用于业务层聊天逻辑统一交互
 * @author guiguan
 * @date 2019/10/23 10:11
 */
@Data
public class ChatContentDto<T> implements Serializable {

    /**
     * 消息id
     */
    @ApiModelProperty(value = "消息id，不需要前端传递，前端传递的msgId在外层")
    private String msgId;

    /**
     * 来源 1-微信客户端 2-顾问
     */
    @ApiModelProperty(value = "来源 1-微信客户端 2-顾问 不需要前端传递")
    private Integer source;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /**
     * 销售员工id
     */
    @ApiModelProperty(value = "销售员工id")
    private Integer saleId;


    /**
     * 消息类型
     */
    @ApiModelProperty(value = "消息类型")
    private String msgType;

    /**
     * 消息内容
     */
    @ApiModelProperty(value = "消息内容")
    private String content;


    @ApiModelProperty(value = "客户端 app类型 1微信服务号 2.微信订阅号 3.微信小程序 4抖音 5自有app")
    private Integer appType;

    /**
     * 原始消息时间
     */
    @ApiModelProperty(value = "原始消息时间")
    private Long msgOriginTime;

    /**
     * 未回复消息的条数（在用户头像左上角显示的数字，仅在用户发送消息的时候才需要）
     */
    @ApiModelProperty("未回复消息的条数（在用户头像左上角显示的数字，仅在用户发送消息的时候才需要）")
    private Long messageCount;
    /**
     * 用户昵称
     */
    @ApiModelProperty(value = "用户昵称")
    private String userNickName;

    @ApiModelProperty(value = "顾问名字")
    private String empName;

    @ApiModelProperty(value = "用户头像")
    private String headImg;

    /**
     * 省份
     */
    @ApiModelProperty(value = "省份")
    private String province;
    /**
     * 性别
     */
    @ApiModelProperty(value = "性别")
    private Integer gender;

    /**
     * 是否是杂志用户
     */
    @ApiModelProperty(value = "是否是杂志用户 true 是 false 不是")
    private Boolean isLowQuality;

    /**
     * 抖音微信映射的 appCode统一的字段
     */
    @ApiModelProperty(value = "抖音微信映射的 appCode统一的字段")
    private String appCode;

    /**
     * 用户openid
     * 微信原始app_code
     * appType 从渠道中心获取
     * appName 从渠道中心获取
     * appId 从渠道中心获取
     */
    @ApiModelProperty(value = "扩展信息 用户openid 微信原始app_code appType appName")
    private T extend;


    @ApiModelProperty(value = "是否加微，true已加微，false未加")
    private Boolean isAddWx;

    @ApiModelProperty(value = "是否无效，true无效，false有效")
    private Boolean isInvalid;

    @ApiModelProperty(value = "是否关注，true关注，false未关注")
    private Boolean isSubscribe;

    @ApiModelProperty(value = "分配方式标签 字典表 1192")
    private String assignWay;

    @ApiModelProperty(value = "是否是新客。true:是，false：否")
    private Boolean newCustomer;

    @ApiModelProperty(value = "线上/线下标签。false:线下 true:线上")
    private Boolean online;

    /**
     * 此消息的channelId
     * 用作消息的同步的时候的排除
     */
    private String currentChannelId;
}
