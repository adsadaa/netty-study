package com.duobaoyu.chatwebsocket.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 *
 *
 * @author guiguan
 * @date 2019/10/12 13:49
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WebSocketResultDto<T> extends WebSocketBaseDto {

    @ApiModelProperty("状态码")
    private String code;

    @ApiModelProperty("信息")
    private String msg;

    @ApiModelProperty("响应消息体")
    private T data;

    @ApiModelProperty("是否成功")
    private Boolean success;
   //todo  是否冗余
    @ApiModelProperty("websocket发送消息服务器的ip")
    private String ip;

    //todo  是否冗余
    @ApiModelProperty("websocket发送消息的channel的id")
    private String channelId;

    @ApiModelProperty("前端定义的消息唯一id，用于接收后端处理结果")
    private String messageId;

    public WebSocketResultDto() {
    }

    public WebSocketResultDto(T data) {
        this.data = data;
    }


    public static WebSocketResultDto success(Object data) {
        WebSocketResultDto webSocketResultDto = new WebSocketResultDto(data);
        webSocketResultDto.setCode("200");
        webSocketResultDto.setSuccess(true);
        return webSocketResultDto;
    }

    public static WebSocketResultDto success() {
        WebSocketResultDto webSocketResultDto = new WebSocketResultDto();
        webSocketResultDto.setCode("200");
        webSocketResultDto.setSuccess(true);
        return webSocketResultDto;
    }

    public static WebSocketResultDto error(String msg) {
        WebSocketResultDto webSocketResultDto = new WebSocketResultDto();
        webSocketResultDto.setCode("500");
        webSocketResultDto.setMsg(msg);
        webSocketResultDto.setSuccess(false);
        return webSocketResultDto;
    }

}
