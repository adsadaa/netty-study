package com.duobaoyu.chatwebsocket.dto;

import lombok.Data;

import java.io.Serializable;


/**
 * 游客信息
 */
@Data
public class UserChannelMsgDto implements Serializable {
    /**
     * 长id
     */
    private String channelId;

    /**
     * 长连接服务器ip
     */
    private String ip;

    /**
     * 登录用户的token
     */
    private String token;

    /**
     * 顾问id
     */
    private Integer empId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * token是否有效
     */
    private Boolean valid;

}
