package com.duobaoyu.chatwebsocket.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用于mq交互的聊天对象
 *
 * @author guiguan
 * @date 2019/10/24 13:45
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WebSocketMqDto<T> extends WebSocketBaseDto {
    /**
     * 长连接ids
     */
    private String channelIds;
    /**
     * 消息体
     */
    private T content;
    /**
     * 此消息的channelId
     * 用作消息的同步的时候的排除
     */
    private String currentChannelId;
    /**
     * 当前服务器的ip
     */
    private String ip;
    /**
     * rocketMq 消息的key，需要与源消息的id一致
     */
    private String key;
    /**
     * 员工id
     */
    private String empId;
    public WebSocketMqDto() {
    }
    public WebSocketMqDto(T content) {
        this.content = content;
    }
}
