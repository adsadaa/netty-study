package com.duobaoyu.chatwebsocket.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 基础dto
 *
 * @author guiguan
 * @date 2019/10/12 13:42
 */
@Data
public class WebSocketBaseDto implements Serializable {

    @ApiModelProperty("发送给谁")
    private String to;

    @ApiModelProperty("由谁发送")
    private String from;

    @ApiModelProperty("事件名称")
    private String event;
}
