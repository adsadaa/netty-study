package com.duobaoyu.chatwebsocket.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 消息交互dto
 *
 * @author guiguan
 * @date 2019/10/12 13:45
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WebSocketMessageDto<T> extends WebSocketBaseDto {

    @ApiModelProperty("来源 1-微信客户端 2-顾问 3-今日头条游客")
    private Integer source;

    @ApiModelProperty("消息内容")
    private T data;

    @ApiModelProperty("前端定义的消息唯一id，用于接收后端处理结果")
    private String messageId;

    public WebSocketMessageDto() {
    }

    public WebSocketMessageDto(T data) {
        this.data = data;
    }


}


