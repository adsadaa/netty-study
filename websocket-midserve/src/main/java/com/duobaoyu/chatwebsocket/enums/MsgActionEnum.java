package com.duobaoyu.chatwebsocket.enums;

/**
 * @author guigu
 */
public enum MsgActionEnum {

    /**
     * 后端监听
     */
    CONNECT("connect", "第一次(或重连)初始化连接"),
    SIGN_VOICE("sign_voice", "签收语音消息"),
    SIGN_ALL("sign_all", "签收除了语音消息之外的所有消息"),
    PING("ping", "前端心跳事件"),
    SALE_MSG("sale_msg", "顾问发送消息"),
    GET_REPLY("get_reply", "获取需要回复的客户"),
    DIS_CONNECT("dis_connect", "断开连接"),
    CLIENT_CONNECT("client_connect","用户端第一次初始化连接"),
    CLIENT_MESSAGE("client_message","头条用户发送消息"),

    /**
     * 前端监听
     */
    CUSTOMER_MSG("customer_msg", "客户的消息"),
    MSG_RESULT("msg_result", "消息响应结果"),
    PONG("pong", "心跳后端响应"),
    NEED_LOGIN("need_login", "需要登录"),
    SYNC_MSG("sync_msg", "多端消息同步"),
    LOGIN_RESULT("login_result", "登录结果"),
    CLIENT_LOGIN_RESULT("client_login_result","游客登录结果"),
    RECEIVE_SALE_MESSAGE("receive_sale_message","顾问发送的消息"),
    SYNC_CLIENT_MSG("sync_client_msg","同步用户自己发送的消息"),
    CLIENT_MSG_RESULT("client_msg_result","用户发送消息结果"),
    LABEL_MSG("label_msg","标签事件"),
    SALE_CLOSE_RECEPTION_SWITCH("sale_close_reception_switch","顾问关闭接客开关"),
    SALE_OPEN_RECEPTION_SWITCH("sale_open_reception_switch","顾问打开接客开关")
    ;

    private final String event;
    private final String content;

    MsgActionEnum(String event, String content) {
        this.event = event;
        this.content = content;
    }

    public String getEvent() {
        return event;
    }

    public String getContent() {
        return content;
    }

    public static MsgActionEnum getByEvent(String event) {
        MsgActionEnum[] valueList = MsgActionEnum.values();
        for (MsgActionEnum v : valueList) {
            if (v.getEvent().equals(event)) {
                return v;
            }
        }
        return null;
    }

    public static void main(String[] args) {
        int length = "AC1010754C9D439F5B3D17C60C32004D".length();
        System.out.println(length);
    }
}
