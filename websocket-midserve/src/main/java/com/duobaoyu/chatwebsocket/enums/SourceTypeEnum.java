package com.duobaoyu.chatwebsocket.enums;

import lombok.Getter;

/**
 * @author gg
 * @version SourceTypeEnum.java, v 0.1 2019-08-17 15:39 gg
 */
@Getter
public enum SourceTypeEnum {

    WE_CHAT("weChat", 1),
    ADVISER("adviser", 2),
    CLIENT("client",3)
    ;

    private int code;
    private String name;

    SourceTypeEnum(String name, int code) {
        this.code = code;
        this.name = name;
    }

}