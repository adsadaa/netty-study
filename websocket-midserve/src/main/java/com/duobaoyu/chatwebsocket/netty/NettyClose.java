package com.duobaoyu.chatwebsocket.netty;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Component;

/**
 * @author guiguan
 * @date 2019/12/20 12:59
 */
@Slf4j
@Component
public class NettyClose implements ApplicationListener<ContextClosedEvent> {

    @Override
    public void onApplicationEvent(ContextClosedEvent contextClosedEvent) {
        try {
            WsServer.getInstance().shutdownGracefully();
            log.info("netty 优雅关闭成功");
        } catch (Exception e) {
            log.error("netty 优雅关闭失败", e);
        }
    }
}
