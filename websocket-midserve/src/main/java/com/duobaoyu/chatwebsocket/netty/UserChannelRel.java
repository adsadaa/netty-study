package com.duobaoyu.chatwebsocket.netty;

import com.duobaoyu.chatwebsocket.dto.EmpChannelMsgDto;
import com.duobaoyu.chatwebsocket.dto.UserChannelMsgDto;
import io.netty.channel.Channel;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author guigu
 * 用户id和channel的关联关系处理
 */
public class UserChannelRel {

    /**
     * 用于记录和管理所有客户端的channel
     * 本项目仅用于监控连接 需手动添加
     * 断开连接时netty自动删除该连接
     */
    public static ChannelGroup users = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);


    /**
     * 用于管理channel 和 顾问的关系
     * 添加时间：connect事件
     * 移除时间：handlerRemoved 顾问退出登录
     */
    public static ConcurrentHashMap<Channel, EmpChannelMsgDto> channelEmpManager = new ConcurrentHashMap<>();

    /**
     * 用于管理channelId 和 channel的关系
     * 添加时间： handlerAdded
     * 移除时间： handlerRemoved
     */
    public static ConcurrentHashMap<String, Channel> channelIdWithChannel = new ConcurrentHashMap<>();

    /**
     * 用于管理userId和channel的关系
     */
    public static ConcurrentHashMap<Long, ArrayList<Channel>> userIdWithChannelList=new ConcurrentHashMap<>();

    /**
     * 用于管理user和Channel的关系
     */
    public static ConcurrentHashMap<Channel, UserChannelMsgDto> channelWithUser=new ConcurrentHashMap<>();

    /**
     * 建立empId与channelId的关系
     */
    public static ConcurrentHashMap<String, List<String>> empIdWithChannelList =new ConcurrentHashMap<>();
}
