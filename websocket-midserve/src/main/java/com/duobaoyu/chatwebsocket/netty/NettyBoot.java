package com.duobaoyu.chatwebsocket.netty;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * netty启动类
 * Spring首次创建上下文时启动
 *
 * @author guigu
 */
@Slf4j
@Component
public class NettyBoot implements ApplicationListener<ApplicationReadyEvent> {

    @Override
    public void onApplicationEvent(@SuppressWarnings("NullableProblems") ApplicationReadyEvent event) {
            try {
                WsServer.getInstance().start();
                log.info("netty 启动成功");
            } catch (Exception e) {
                log.error("netty启动失败", e);
            }
    }
}
