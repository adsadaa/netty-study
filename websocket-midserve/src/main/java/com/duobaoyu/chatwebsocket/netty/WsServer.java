package com.duobaoyu.chatwebsocket.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author guigu
 */
@Slf4j
@Component
public class WsServer {

    private ServerBootstrap server;

    private EventLoopGroup mainGroup;

    private EventLoopGroup subGroup;

    private static class SingletonWsServer {
        static final WsServer INSTANCE = new WsServer();
    }

    /**
     * 将server做成单例
     *
     * @return WsServer
     */
    public static WsServer getInstance() {
        return SingletonWsServer.INSTANCE;
    }


    public WsServer() {
        mainGroup = new NioEventLoopGroup();
        subGroup = new NioEventLoopGroup();
        server = new ServerBootstrap();
        server.group(mainGroup, subGroup).channel(NioServerSocketChannel.class).childHandler(new WsServerInitializer());
    }

    public void start() throws InterruptedException {
        ChannelFuture future = server.bind(8088);
        //服务器的channel，可以通过这个channel进行消息交互
        Channel serverChannel = future.channel();
        //监听关闭的channel，设置为同步方式
        serverChannel.closeFuture().sync();
        System.err.println("netty websocket server 启动完毕...");
    }

    public void shutdownGracefully() {
        log.warn("netty 优雅关闭");
        mainGroup.shutdownGracefully();
        subGroup.shutdownGracefully();
        UserChannelRel.users.close().awaitUninterruptibly();
    }

}
