package com.duobaoyu.chatwebsocket.feign;

import com.duobaoyu.middleware.common.Result;
import com.duobaoyu.systemcenter.model.result.ChatUserCacheResultDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author gg
 * @version SystemCenterFeign.java, v 0.1 2019-08-13 17:11 gg
 */
@FeignClient(value = "system-center")
public interface SystemCenterFeign {


    /**
     * 以员工工号获取用户，用户员工身份基本信息
     *
     * @param token token
     * @return Result
     */
    @GetMapping(value = "/chat/getChatUserBasicMessageByToken.do")
    @ApiOperation("以员工工号获取用户，用户员工身份基本信息")
    Result<ChatUserCacheResultDto> getChatUserBasicMessageByToken(@ApiParam(name = "用户token", required = true)
                                                             @RequestParam(value = "token") String token);


}